<?php

/**
 * @file
 * This file adds a wizard to help the user initialize the interface
 */

/**
 * Implementation of hook_perm().
 */
function semweb_permission() {
  return array(
    'access interface' => array(
      'title' => t('Access the interface'),
      'description' => t('Allow access to the SemWeb interface provided.'),
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function semweb_menu() {
  
  $items['admin/structure/semweb'] = array(
    'title' => 'Semantic Interface',
    'description' => 'Use an experimental semantic web interface.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('semweb_ui'),
    'access arguments' => array('access interface'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Build the form.
 * Actually, this uses sub functions to help divide the phases of the form.
 */
function semweb_ui($form, &$form_state) {
  $phase = &$form_state['phase'];
  $form = _semweb_ui_form_1($form,&$form_state);
  if ($phase >= 2) {
    $form = _semweb_ui_form_2($form, &$form_state);
  }
  return $form;
}

/**
 * Validate the form.
 * Switch based on the phase to sub functions.
 */
function semweb_ui_validate($form, &$form_state) {
  /**
   *  Don't execute anything until basic Form API rules are observed.
   * @link http://api.drupal.org/api/drupal/includes%21form.inc/function/form_get_errors/7#comment-18059
   */
  if (form_get_errors()) {
    return;
  }
  $phase = &$form_state['phase'];
  switch ($phase) {
    case 1:
      $result = _semweb_ui_validate_1($form, &$form_state);
      break;
    case 2:
      $result = _semweb_ui_validate_2($form, &$form_state);
      break;
  }
  if (isset($result)) {
    return $result;
  }
}

/**
 * Submit the form.
 * Switch based on the phase to sub functions.
 */
function semweb_ui_submit($form, &$form_state) {
  $phase = &$form_state['phase'];
  switch($phase) {
    case 1:
      $result = _semweb_ui_submit_1($form, &$form_state);
      break;
    case 2:
      $result = _semweb_ui_submit_2($form, &$form_state);
      break;
  }
  if (isset($result)) {
    return $result;
  }
}

/**
 * Build page 1 of the form.
 */
function _semweb_ui_form_1($form, &$form_state) {

  $phase = &$form_state['phase'];

  if (!isset($phase)) {
    $phase = 1;
  }
  
  // Allow a way to output dpm's from earlier in form processing.
  if (!empty($form_state['dpm'])) {
    foreach($form_state['dpm'] as $key => $value) {
        $form[$key] = array(
          '#markup' => $value,  
        );
    }
  }

  // Now create a textfield for the address of the rdf file to start from.
  // @TODO Check to see if a url specific form field is installed.
  $form['raw_rdf'] = array(
    '#tree' => true,
    '#type' => 'fieldset',
    '#description' => t('Enter one !uri to access a !rdf file to ' .
      'start from.', array(
        '!uri' => '<abbr title="Uniform Resource Identifier">URI</abbr>',
        '!rdf' => '<abbr title="Resource Description Framework">RDF</abbr>',
      )),
    '#title' => t('Raw RDF Files'),
    '#collapsible' => TRUE,
    '#collapsed' => ($phase != 1 ? TRUE : FALSE),
  );

  if (isset($form_state['input']['raw_rdf'])) {
    $raw_rdf = $form_state['input']['raw_rdf'];
  }

  $form['raw_rdf']['address'] = array(
    '#type' => 'textfield',
    '#field_prefix' => 'http://',
    '#default_value' => 
                   (isset($raw_rdf['address']) ? $raw_rdf['address'] : ''),     
    '#title' => t('RDF file location'),
    '#parent' => 'raw_rdf',
    '#required' => ($phase != 1 ? FALSE : TRUE),
    '#weight' => '0',
  );

  $form['back'] = array(
    '#type' => 'submit',
    '#submit' => array(),
    '#validate' => array(),
    '#value' => t('Cancel'),
    '#weight' => '1',
  );
  $form['next'] = array(
    '#type' => 'submit',
    '#value' => t('Next >>'),
    '#weight' => '2',
  );
  
  dpm($form_state,'form_state');

  return $form;
}

/**
 * Build page 2 of the form.
 */
function _semweb_ui_form_2($form, &$form_state) {
  return $form;
}

/**
 * Validate page 1 of the form.
 */
function _semweb_ui_validate_1($form, &$form_state) {
  $url = semweb_validate_url($form_state['input']['raw_rdf']['address']);
  
  if (form_get_errors()) {
      return;
  }
  // Save the values for later use.
  $form_state['values']['origin'][] = $url;
  $form_state['dpm'][] = dpm($form_state,'validation');
}

/**
 * Validate the url field.
 * 
 * @param string &$url
 * The url given in the form.
 * @return Two possibilities:
 * On success, the effective url. On failure, an error message on the field. 
 */
function semweb_validate_url(&$url) {
  // If no protocol is listed, presume 'http://'
  if (strpos($url,'//') === FALSE) {
    $url = 'http://' . $url;
  }
  // Pass through API functions to ensure safety.
  $url = check_url($url);
  // Get the headers.
  $status = semweb_check_headers(&$url);
  // Confirm the status
  if (!($status < 400) || ($status < 200)) {
      return form_set_error('rdf][address', 
              t('Address does not resolve to a location, please verify it.'));
  }
  // Save the url for later use.
  return $url;
}

/**
 * Validate page 2 of the form.
 */
function _semweb_ui_validate_2($form, &$form_state) {
  
}

/**
 * Submit page 1 of the form.
 */
function _semweb_ui_submit_1($form, &$form_state) {
  $url = $form_state['values']['origin'];
  /**
   *  In order to stop any of these operations, you can remove the leading
   * '//' from the explanation of the operation prior.
   */
  // If the structure for one of the origins hasn't been loaded yet, load it.
  foreach($url as $key => $value) {
    if (!isset($form_state['values']['rdf']['structure'][$key])) {
      // /* Order the elements so it makes sense...
      $form_state['values']['rdf']['structure'][$key] = array(); // */
      $contents = semweb_get_remote($value);
      // /* Take the comments out, since this interface will be remaking the code.
      $contents = semweb_parse_remove_comments($contents); // */
      $structure = semweb_parse_rdf_get_structure($contents);
      $form_state['values']['rdf']['raw'][] = $contents;
      $form_state['values']['rdf']['structure'][$key] = $structure;
    }
  }
  $form_state['dpm'][] = dpm($form_state, 'semweb_ui_submit_1, form_state');
}

/**
 * Submit page 2 of the form.
 */
function _semweb_ui_submit_2($form, &$form_state) {
  
}

/**
 * Get a file from a remote url and return the contents as a string for further
 * parsing
 * 
 * @param string $url
 * The url to access the remote content at.
 * @return string $content
 * The contents of the file at the url. Returns FALSE on error.
 */
function semweb_get_remote($url) {
  $status = semweb_check_headers(&$url);
  if (!($status < 400) || $status < 200) {
    return FALSE;
  }
  return file_get_contents($url);
}

/**
 * A way to check the headers of a url.
 * 
 * @param string &$url
 *   Will be changed to the location redirected to, if any.
 * @param boolean $debug = FALSE
 *   Whether to display diagnostic information prior to return.
 * @return integer $status
 *   The numeric code indicating the status of the final url.
 */
function semweb_check_headers(&$url, $debug = FALSE) {
  $headers = get_headers($url,1);
  // Check if redirects are an issue.
  if (isset($headers['Location'])) {
      // Single redirect
    if (!is_array($headers['Location'])) {
      $status = substr($headers[1],9,3);
      $url = $headers['Location'];
    } else { // Multiple redirects.
      $count = count($headers['Location']);
      $status = substr($headers[$count],9,3);
      unset($count);
      $url = array_pop($headers['Location']);
    }  
  } else { // No redirects...
      $status = substr($headers[0],9,3);
  }
  $status += 0;
  if ($debug && in_array('devel', module_list())) {
    dpm($status,'semweb_check_headers --> status');
    dpm($url, 'semweb_check_headers --> url');
    dpm($headers,'semweb_check_headers --> headers');
  }
  return $status;
}