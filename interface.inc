<?php
/**
 * @file
 * This is where the interface is built and handled, in terms of PHP anyway
 */


/**
 * Prior to getting into the forms, we need conditional functions to handle the
 * multple phases (form, validate and submit) for each page of the form.
 *
 * First the builing conditional
 */
function semweb_ui($form, &$form_state) {
  // Essentially, we want to switch to another function to keep things simple
  $form = semweb_ui_1($form, $form_state);
  // If the above has already been submitted, then modify it further...
  if (!empty($form_state['step_num']) && $form_state['step_num'] != 1) {
    $form = semweb_ui_2($form, $form_state);
  }
  // Build the form now.
  return $form;
}

/**
 * Similar to the above, but for the validation callbacks.
 */
function semweb_ui_validate($form, $form_state) {
  if (empty($form_state['step_num']) || $form_state['step_num'] == 1) {
    semweb_ui_1_validate($form, $form_state);
  }
  else {
    semweb_ui_2_validate($form. $form_state);
  }
}

/**
 * Again, same as above, but for submit callbacks
 */
function semweb_ui_submit($form, &$form_state) {
  if (empty($form_state['step_num']) || $form_state['step_num'] == 1) {
    semweb_ui_1_submit($form, &$form_state);
  }
  else {
    semweb_ui_2_submit($form, &$form_state);
  }
}


function semweb_ui_2($form, &$form_state) {
  dpm($form_state);
  return $form;
}

function semweb_ui_1($form, &$form_state) {
  dpm($form_state,'form_state');
  
  // First set the step number
  $form_state['step_num'] = 1;

  // Allow a way to output dpm's from earlier in form processing.
  if(!empty($form_state['dpm'])) {
    $form['markup'] = array(
	  '#markup' => $form_state['dpm']
	);
  }

  // Now create two textfields for the address of each rdf file
  // @TODO Check to see if a url specific form field is installed.
  $form['raw_rdf'] = array(
    '#tree' => true,
	'#type' => 'fieldset',
	'#description' => t("Enter one !uri to access a !rdf file ".
	  "to update or extend.", array(
	    '!uri' => '<abbr title="Uniform Resource Identifier">URI</abbr>',
		'!rdf' => '<abbr title="Resource Description Framework">RDF</abbr>'
	  )),
    '#title' => t('Raw RDF Files'),
	'#collapsible' => TRUE,
	'#collapsed' => ($form_state['step_num'] != 1 ? TRUE : FALSE),
  );

  if (isset($form_state['values']['raw_rdf'])) {
    $raw_rdf = $form_state['values']['raw_rdf'];
  }

  $form['raw_rdf']['address'] = array(
    '#type' => 'textfield',
    '#field_prefix' => 'http://',
    '#default_value' => isset($raw_rdf['address']) ? $raw_rdf['address'] : '',
    '#title' => t('RDF file location'),
    '#parent' => 'raw_rdf',
    '#required' => TRUE,
	'#weight' => '0',
  );

  $form['back'] = array(
    '#type' => 'submit',
	'#submit' => array(),
	'#validate' => array(),
	'#value' => t('Cancel'),
	'#weight' => '1',
  );
/*
  $form['add_rdf'] = array(
    '#type' => 'submit',
    '#value' => t('Add another RDF file'),
    '#submit' => array('semweb_ui_add_rdf'),
  );  
*/
  $form['next'] = array(
    '#type' => 'submit',
	'#value' => t('Next >>'),
	'#weight' => '2',
	#'#submit' => array('semweb_ui_first_process'),
	#'#validate' => array('semweb_ui_first_validate'),
  );

  return $form;
}

/**
 * Validate page 1.
 *
 * Written in such a way for the add more button could be add to the form.
 */
function semweb_ui_1_validate($form, &$form_state) {
}

/**
 * Process the given rdf's into tables
 */
function semweb_ui_1_submit($form, &$form_state) {
  $address = $form_state['values']['raw_rdf']['address'];
  // @TODO Expand this to include additional protocols (https:// compes to mind)
  if (substr_compare($address, 'http://', 0, 7) == 0) {
    $address = substr($address,7);
  }
  // Hold the values in one array.
  $file = array(
    'address' => "http://$address",
  );
  // Maintain the value in the form.
  $form_state['input']['raw_rdf']['address'] = $address;

  $result = semweb_get_file($file['address']);

  dpm($result);

  if(is_string($result)) {
    return form_set_error('raw_rdf][address',$result);
  }
  else {
    $file['raw'] = $result[0];
  }

  $file['rdf'] = semweb_get_rdf($file['raw']);
  $file['source'] = semweb_rdf_sources(&$file['rdf']);
  $msg = "The following array is made from the file designated; and formatted as follows:\r\t\"address\" => The original url entered on this form.\r\t\"raw\" => The file designated, without the comments nor line breaks.\r\t\"rdf\" => an array of uri's pointing to url's.\r\t\"source\" => An array of the source of the rdf definitions. Right now, it is not guarenteed to be functional, as it sometimes returns an HTML page instead of an RDF page, with all the information needed, to be parsed later.";
  dsm($msg);
  dpm($file, '$file');
  $form_state['files'][$address] = $file;
  $form_state['step_num']++;
}

function semweb_get_fields($file, $debug = FALSE) {
  $raw = $file['raw'];
  $raw = explode('<',$raw);
  $rdf = $file['rdf'];
  $uris = array_flip($rdf);
  foreach($raw as $key=>$value) {
    if(strpos($value,'/') !== 0) {
      $token[] = before('>',$value,FALSE);
	}
  }
  foreach($token as $key=>$value) {
    if(strpos($value,' ')!==FALSE) {
      $token[$key] = explode(' ', $value);
	}
  }
  $result = _semweb_untokenize($token);
  // Remove excess values
  foreach($result as $key=>$value){
    if(!in_array($key,$uris) || is_numeric($key)) {
      unset($result[$key]);
	}
	else {
      $result[$key] = array_unique($value);
	}
  }
  if($debug) {
    dpm($result);
  }
  return $result;
}

function _semweb_untokenize($tokens) {
  $result = array();
  foreach($tokens as $value) {
    if(is_array($value)) {
      $result = array_merge(_semweb_untokenize($value),$result);
	}
	else{
      $key = before(':',$value,FALSE);
	  if(strpos($value,'=')!==FALSE) {
        $field = between(':','=',$value,FALSE);
  	  }
	  else {
        $field = after(':',$value,FALSE);
	  }
      $result[$key][] = $field;
    }
  }
  return $result;
}

/**
 * Does the file retrieval and removes the comments. Also, confirms
 * internet access.
 * @param $address
 *  The uri of the rdf file.
 * @return
 *  string if an error happened, an array containing a string if success.
 */
function semweb_get_file($address) {
  // First, confirm internet connection.
  // First confirm general internet access
  if(_semweb_verify_internet() == FALSE) {
    return 'An internet conenction cannot be established, confirm the settings'.
	  'on the server, then try again.';
  }
  // Quickly confirm access to the uri provided.
  $header = semweb_header_check($address);
  if(!is_numeric($header)) {
    return $header;
  }

  // Get contents
  $raw = file_get_contents($address);

  //filter out comments
  while(strpos($raw,'<!--')!==FALSE) {
    $start = strpos($raw,'<!--');
	$stop = strpos($raw,'-->',$start)+3;
    if($stop === FALSE) {
      return array(substr($raw,$start));
	}
	$length = $start - $stop;
	$raw = substr($raw,0,$start) . substr($raw,$stop);
  }

  return array($raw);
}

/**
 * Get the source of the rdf definitions here.
 * This should be all the accessory information needed for the interface.
 *
 * @param $rdf
 *  An array, keys being the uri's, values being the url's, as per the rdf.
 * @return array $sources
 *  An array of the content of the sources, keyed by the uri's, and excluding
 *  comments.
 */
function semweb_rdf_sources(&$rdf) {
  if(!is_array($rdf)) {
    // Lack of a viable arg, so return nothing.
	return;
  }
  $sources = array();
  foreach($rdf as $uri => $url) {
	$headers = get_headers($url);
	$headers = array_filter($headers, '_semweb_filter_header');
	if (substr(current($headers),-6,3) == 200) {
      continue;
	}
	$keys = array_keys($headers);
	/**
	 * Get the HTTP response of the final request, if its above 400, ignore it for now.
	 * Otherwise get the location as the $url in the $rdf.
	 * @todo display error message to user.
	 */
	$code = substr(array_pop($headers),9,3);
	if ($code >= 400) {
      unset($rdf[$uri]);
	}
	else {
      $rdf[$uri] = substr(array_pop($headers),10);
	}
  }
  $sources = array();
  // Now that we've determined the locations, get the source, using the same method as in the
  foreach($rdf as $uri => $url) {
    $raw = file_get_contents($url);
	// Filter out the comments...
    while(strpos($raw,'<!--')!==FALSE) {
	  $start = strpos($raw,'<!--');
	  $stop = strpos($raw,'-->',$start)+3;
	  if($stop === FALSE) {
	    return array(substr($raw,$start));
	  }
	  $length = $start - $stop;
	  $raw = substr($raw,0,$start) . substr($raw,$stop);  
	}
	$sources[$uri] = $raw;
  }
  return $sources;
}

function _semweb_filter_header($var) {
  if (substr($var,0,4) == 'HTTP') {
    return TRUE;
  }
  if (substr($var,0,9) == 'Location:') {
    return TRUE;
  }
  return FALSE;
}

/**
 * The logic to extract the rdf definitions from the contents.
 *
 * @param $contents
 *   A string of the contents of a given rdf file.
 * @return array
 *  An array, keyed on the abbreviations, pointing to the full uri.
 */
function semweb_get_rdf($contents, $debug = FALSE) {
  $rdf = between('<rdf:RDF','>',$contents,FALSE);
  $rdf = explode(' ',$rdf);
  $keys = array();
  $values = array();
  foreach($rdf as $key => $value) {
    $start = substr($value,0,6);
	if(substr($value,0,6) == 'xmlns:') {
      $keys[] = between(':','=',$value);
	  $values[] = substr(after('="',$value,FALSE),0,-1);
	}
  }
  $result = array_combine($keys,$values);
  if ($debug) {
    dsm('semweb_get_rdf() debug');
	dpm($rdf, 'rdf');
	dpm($keys,'keys');
	dpm($values, 'values');
	dpm($result,'result');
  }
  return $result;
}

/**
 * A way to get the specific url's of the source documents for the information
 * to use in constructuing the interface.
 *
 * @param array $rdf
 *  This is an array of keys to url's to try and access the schematic at.
 * @param boolean $debug = FALSE
 *  This outputs extra information for debuging purposes.
 */
function semweb_get_rdf_source($rdf = array(), $debug = FALSE) {
  foreach($rdf as $uri => $initalURL) {
    $header = get_headers($initialURL);
	if($debug) {
      dpm($header,$uri);
	}
  }
}

function semweb_between($this,$that,$inthat,$check_plain = TRUE) {
  $content = before($this,$inthat,FALSE);
  $content = after($that,$content,FALSE);
  if(check_plain) {
    $content = check_plain($content);
  }
  return $content;
}

function semweb_remove_multi($this,$fromthat,$check_plain = TRUE) {
  if($check_plain) {
    $fromthat = check_plain($fromthat);
  }
  if(is_array($this)) {
    if(isset($this[0],$this[1])) {
      while(strpos($fromthat,$this[0])!==FALSE) {
        $fromthat = semweb_remove($this,$fromthat,FALSE);
	  }
	  return $fromthat;
	}
    return;
  }
  while(strpos($fromthat,$this)!==FALSE) {
    $fromthat = semweb_remove($this,$fromthat,FALSE);
  }
  return $fromthat;
}

/**
 * A utility function to aid with removing segments of text.
 *
 * @param $this
 *  The item to remove. If its a string, that describes it,
 *  If an array, it will remove that which is surrounded by the first
 *  two elements.
 * @param $fromthat
 *  The string to remove items from.
 * @param $check_plain
 *  Boolean on if this should be run through check_plain.
 * @return string lacking the piece described through the parameters.
 */
function semweb_remove($this,$fromthat,$check_plain = TRUE) {
  if($check_plain) {
    $fromthat = check_plain($fromthat);
  }
  if(is_array($this)) {
    if (isset($this[1],$this[0])) {
      $pos[0] = strpos($fromthat,$this[0]);
	  $pos[1] = strpos($fromthat,$this[1],$pos[0]) + strlen($this[1]);
	  return substr($fromthat,$pos[0],$pos[1]);
	}
	return;
  }
  return substr($fromthat,strpos($fromthat,$this),strlen($this));
}
/**
 * A way to handle remove a segment of a string.
 *
 * @param $before
 *  The string to start the removed segment.
 * @param $after
 *  The sting to end the removed segment.
 * @param $content
 *  The string to process.
 * @param $switches
 *  An array of optuions.
 *    Values taken into consideration:
 *      'clean' - runs the content though check_plain
 *      'debug' - outputs debug information
 * @return $result
 *  A string lacking the removed content.
 */
function semweb_remove_old($before, $after, $content, $switches = array('clean')){
  if(in_array('clean',$switches)) {
    $content = check_plain($content);
  }
  $start = strpos($content,$before);
  $stop = strpos($content,$after,$start) + strlen($after);
  $result = substr($content,0,$start) . substr($content,$stop);
  if(in_array('debug',$switches)) {
    dpm($result);
  }
  return $result;
}

/**
 * A helper function to remove comments from an RDF file. This helps avoid
 * interpretting things not applicable in the real application of it.
 */
function semweb_remove_comments($contents, $debug = FALSE) {
  while(strpos($contents,'<!--') !== FALSE) {
    $contents = before('<!--',$contents) . after('-->',$contents);
  }
  if ($debug) {
    dpm($contents);
  }
  return $contents;
}

/*
 * A quick way to verify an internet connection exists.
 *
 * The presence of a server does not mean that internet is
 * available to it, so this verifies that.
 *
 * @param $force - This will force the check to proceed, if TRUE.
 * @return boolen - TRUE if internet is present, FALSE otherwise.
 */
function _semweb_verify_internet($force = FALSE) {
  global $semweb_connection_error;
  if(!isset($semweb_connection_error) || $force == TRUE) {
	$semweb_connection_error = 0;
  }
  elseif ($semweb_connection_error > 0) {
    return TRUE; // based on past results, return TRUE.
  }
 
  // Proceed with the check.
  set_error_handler('semweb_connection_error');

  file('http://www.google.com');

  $semweb_connection_error+=5;

  restore_error_handler();

  if ($semweb_connection_error > 0) {
    return TRUE;
  }
  return FALSE;
}

/*
 * An error handler used above. This hides nasty messages an
 * makes it clear to the code that the internet connection failed.
 */
function semweb_connection_error() {
  global $semweb_connection_error;
  $semweb_connection_error = -100;
}

/*
 * A quick way to confirm that a resource exists. If it does work (has a status
 * of 2xx) then that status code is returned. Otherwise, an error message is
 * returned for output through form_set_error().
 *
 * @param $uri - the internet address to check the headers of.
 * @return $status - described above.
 */
function semweb_header_check($uri) {
  $headers = get_headers($uri,1);
  $status = substr($headers[0],9,3);
  if($status>300 && ($status < 304 || $status == 307)) {
    if(isset($headers['Location'])) {
      return t('Redirect detected, please confirm !location is the actual '.
	  'address of the content.', array('!location' => $headers['Location']));
	}
  }
  if ($status >= 200 && $status < 300) {
    return $status;
  }
  return t('There was some issue with the url provided, please confirm and try'.
    ' again.');
}

