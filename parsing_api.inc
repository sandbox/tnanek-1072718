<?php
/**
 * @file
 * Parsing functions for semweb to aid in the lack of parsing_api module.
 */

/**
 * Get the text after a given string.
 *
 * @param $this
 *  The string to search for.
 * @param $inthat
 *  The string to search in.
 * @check_plain = TRUE
 *  Whether to check the result is plain or not.
 * @return the string after $this in $inthat.
 */
function after($this, $inthat, $check_plain = TRUE) {
  if (!is_bool(strpos($inthat, $this))) {   
	if (!$check_plain) {   
	  return substr($inthat, strpos($inthat,$this)+strlen($this));
	}   
  return check_plain(substr($inthat, strpos($inthat,$this)+strlen($this)));
  }   
}

/**
 * Get the text after the last occurance of a given string.
 *
 * @param $this
 *  The string to search for the last of.
 * @param $inthat
 *  The string to search in.
 * @param $check_plain = TRUE
 *  Whether to check the result is plain or not.
 * @return the string after the last of $this from $inthat.
 */
function after_last($this, $inthat, $check_plain = TRUE) {
  if (!is_bool(strrevpos($inthat, $this))) {   
    if (!$check_plain) {   
	  return substr($inthat, strrevpos($inthat, $this)+strlen($this));
	}   
	return check_plain(substr($inthat, strrevpos($inthat, $this)+strlen($this)));
  }   
}

/**
 * Get the text before a given string.
 *
 * @param $this
 *  The string to search for.
 * @param $inthat
 *  The string to search through.
 * @check_plain = TRUE
 *  Whether to check the result is plain or not.
 * @return the string before the first occurance of $this from $inthat.
 */
function before($this, $inthat, $check_plain = TRUE) {
  if (!$check_plain) {   
	return substr($inthat, 0, strpos($inthat, $this));
  }   
  return check_plain(substr($inthat, 0, strpos($inthat, $this)));
}

/**
 * Get the text before the last occurance of a given string.
 *
 * @param $this
 *  The string to search for.
 * @param $inthat
 *  The string to search through.
 * @param $check_plain = TRUE
 *  Whether to check the result is plain or not.
 * @return the string before the last occurance of $this from $inthat.
 */
function before_last($this, $inthat, $check_plain = TRUE) {
  if (!$check_plain) {   
	return substr($inthat, 0, strrevpos($inthat, $this));
  }   
  return check_plain(substr($inthat, 0, strrevpos($inthat, $this)));
}

/**
 *
 */
function strrevpos($haystack, $needle) {
  if (version_compare(phpversion(), '5.0') < 0) {   
	// php4 doesn't support strrpos for strings, but it does for a single character
	$rev_pos = strpos(strrev($haystack), strrev($needle));
	if ($rev_pos === FALSE) {   
	  return FALSE;
	}   
	return strlen($haystack) - $rev_pos - strlen($needle);
  }   
  // php5 does support strrpos for strings
  return strrpos($haystack, $needle);
}
